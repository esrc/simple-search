/**
 * This file is subject to the terms and conditions defined in the
 * 'LICENSE.txt' file, which is part of this source code package.
 */

'use strict';

/*---------------------------------------------------------------------------*/
/* SimpleSearch Application                                                  */

var app = angular.module('simplesearch',['Directives','Filters','Solr','Utils']);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
        when('/:query', { event: "/query" }).
        otherwise({ event: "/" });
}]);
