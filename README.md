Simple Search AJAX Document Search Interface to Apache Solr/Lucene
==================================================================

SOLR-AJAX is a single page, Javascript web application for searching and
presenting document search data from an Apache Solr/Lucene search index. 
The project emphasizes ease of client side implementation.

A live demo is available online at https://web.esrc.unimelb.edu.au/ECOM/.


Credits
-------

SimpleSearch is a project of the eScholarship Research Center at the University of
Melbourne. For more information about the project, please contact us at:

  > eScholarship Research Center
  > University of Melbourne
  > Parkville, Victoria
  > Australia
  > www.esrc.unimelb.edu.au

Authors:

 * Davis Marques <davis.marques@unimelb.edu.au>
 * Marco La Rosa <marco@larosa.org.au>

Thanks:

 * Angular.js - http://www.angularjs.org
 * JQuery - http://www.jquery.com
 * JQuery UI - http://www.jqueryui.com


Build, Install
--------------

scripts/compile.sh will build deployable simplesearch.min.js, simplesearch.css,
simplesearch.min.css files for you. Source Javascript files are downloaded to a
temporary directory from the eaccpf-ajax project on Bitbucket. Selected components
are then extracted and compiled together to form the simplesearch application.

Instructions for deploying the application on your web site are found in the
index.html file.


License
-------
See the LICENSE.txt file for copyright and license information.


Version History
---------------

0.1.3

 * Updated the build script to pull application components from eaccpf-ajax
   project
 * Updated documentation to try and make the setup process clearer, simpler

0.1.2

 * IE 7 does not provide console.log service. Wrapped logging calls in a check
   for window.console object
 * Added HTML5 polyfill to sample search page
 * Modified compiled script to build json2/3 polyfill into the
   simplesearch.min.js file
 * Updated documentation

0.1.1

 * Replaced search.html with contents of results.html
 * Added documentation to search.html in the form of inline comments
 * Modified SOLR_BASE config variable to default to /solr, such that it will
   use the current host /solr path to access the index

0.1.0

 * Forked implementation from Solr-Ajax
