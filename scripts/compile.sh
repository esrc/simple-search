#!/bin/bash
# This script compiles the SimpleSearch application. The application includes
# all components required to execute a keyword based search against an Apache
# Solr/Lucence index and present results to the user.

# Absolute path to this script. /home/user/bin/foo.sh
SCRIPT=$(readlink -f $0)

# Absolute path this script is in. /home/user/bin
SCRIPTPATH=`dirname $SCRIPT`

# absolute path to the project folder
PROJECT=`dirname $SCRIPTPATH`

# -----------------------------------------------------------------------------
# compile simplesearch.css

OUTPUT="$PROJECT/app/css/simplesearch/simplesearch.min.css"

cat /dev/null > $OUTPUT
echo "/* WARNING: THIS FILE IS GENERATED AUTOMATICALLY. CHANGES MADE TO THIS FILE WILL NOT PERSIST. */" >> $OUTPUT
cat $PROJECT/app/css/jquery-ui/jquery-ui.min.css >> $OUTPUT
cat $PROJECT/app/css/simplesearch/simplesearch.css >> $OUTPUT

SIZE=$(stat -c%s "$OUTPUT")
echo "Wrote $OUTPUT"
echo "$SIZE bytes"

# -----------------------------------------------------------------------------
# update the local copy of eaccpf-ajax

REPO="$PROJECT/tmp/eaccpf-ajax"
cd $REPO
git pull origin master
git pull origin production

# -----------------------------------------------------------------------------
# compile simplesearch.js

OUTPUT="$PROJECT/app/js/simplesearch/simplesearch.min.js"
TEMP="$PROJECT/app/js/simplesearch/simplesearch.min.js.tmp"

cat /dev/null > $OUTPUT
cat /dev/null > $TEMP

echo "/* WARNING: THIS FILE IS GENERATED AUTOMATICALLY. CHANGES MADE TO THIS FILE WILL NOT PERSIST. */" >> $OUTPUT

# json2 polyfill for IE 7/8
cat $PROJECT/app/js/json-js/json3.min.js >> $OUTPUT

# jquery
cat $PROJECT/app/js/jquery/jquery-1.9.1.min.js >> $OUTPUT

# jquery-ui
cat $PROJECT/app/js/jquery-ui/jquery-ui-1.10.3.custom.min.js >> $OUTPUT

# angularjs
cat $PROJECT/app/js/angular/angular.min.js >> $OUTPUT

# simplesearch
tail -n +7 $REPO/app/js/solr-ajax/controllers/DocumentSearchResultsController.js >> $TEMP
tail -n +7 $REPO/app/js/solr-ajax/controllers/SearchBoxController.js >> $TEMP
tail -n +7 $REPO/app/js/solr-ajax/directives/autocomplete.js >> $TEMP
tail -n +7 $REPO/app/js/solr-ajax/filters/textfilters.js >> $TEMP
tail -n +7 $REPO/app/js/solr-ajax/services/solr.js >> $TEMP
tail -n +7 $REPO/app/js/solr-ajax/services/utils.js >> $TEMP
tail -n +7 $PROJECT/app/js/simplesearch/app.js >> $TEMP

# rename DocumentSearchResultsController to SearchResultsController
sed -i 's|DocumentSearchResultsController|SearchResultsController|g' $TEMP

# rename SearchBoxController to SearchFormController
sed -i 's|SearchBoxController|SearchFormController|g' $TEMP

# print file stats to the console
SIZE=$(stat -c%s "$TEMP")
echo "Temp file simplesearch.js.tmp ($SIZE bytes)"

cat $TEMP >> $OUTPUT
rm $TEMP

SIZE=$(stat -c%s "$OUTPUT")
echo "Wrote $OUTPUT "
echo "$SIZE bytes"

# -----------------------------------------------------------------------------
# minify simplesearch.css and simplesearch.js

